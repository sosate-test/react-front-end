> # Brandon Henao


## Despliege
El despliegue se hace a traves de [vercel](https://vercel.com/) y con integracion continua con gitLab, esta es la url base donde podemos ver desplegado nuestro [cliente web](https://react-front-end-alpha.vercel.app/).

> **Note:** el servicio esta alojado en el servidor con una cuenta gratuita , por ende puede tener retrasos o incluso perdida de la informacion


### Demo
En la imagen se demuestra como podemos empezar a usar nuestra aplicación.  

![demo](src/assets/demo.gif)


## Herramientas
- Visual studio code
- LICEcap


## Tecnologías
- React
- material-ui
- Pusher
- react-widgets/lib/Multiselect
- redux
- react-toastify [notificaciones](https://fkhadra.github.io/react-toastify/introduction)
- react-rating
- typescript
- link-module-alias, para rutas absolutas

se debe de colocar en el package.json algo como esto
```sh
    {
        "_moduleAliases": {
            "@componentes": "dist/componentes",
            "@servicios": "dist/servicios"
        }
    }
```

- react-google-maps , se debe de generar un api key para consumir estos servicios.
[Google Console](https://console.cloud.google.com/)



## Comandos

    npm start -> para ejecutar nuetsri proyecto
    npm run postinstall -> para poder instalar las rutas absolutas 
    para crear un proyecto para produccion, usamos npm run build.
    