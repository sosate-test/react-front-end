/** Importaciones de componentes externos */
import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';



/**Importar redux */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { accionCandidates } from '@redux/accions'

import Lista from './ListItem'


interface Props {
    classes: any,
    actions: any,
    candidates: any
}
interface State {
}


class Step1 extends Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
        }

    }



    render() {
        const { classes, candidates } = this.props;

        return (
            <Grid container className={classes.root}>
                <div className={classes.container}>


                    <p className={classes.text} >
                        Señor usuario a continuacion te presentaremos los candidatos que has seleccionado , recuerda que debes calificar a todos los candidatos.
                    </p>

                    {
                        candidates.candidatesCalifated.candidates.map((item: any, i: number) => {
                            return this.renderCandidates(item, i)
                        })
                    }


                </div>
            </Grid>
        )
    }

    renderCandidates = (item: any, i: number) => {
        return (
            <Lista index={i} item={item} />
        )
    }


}



const useStyles = () => ({
    root: {
        //width: '100%',
        justifyContent: "center",
    },
    container: {
        width: '80%',
        background: 'transparent',
        alignItems: "center"
    },
    containerItems: {
        "display": "flex", 
        "flex-direction": "row", 
        "justifyContent": "flex-start"
    },
    text: {
        fontSize: 14,
        paddingBottom: "3%"
    },
});



const mapStateToProps = (state: any) => {
    return {
        candidates: state.candidates
    }
}

//const mapDispatchToProps = { accionCandidates }
const mapDispatchToProps = (dispatch: any) => {
    return {
        actions: {
            AccionCandidates: bindActionCreators(accionCandidates, dispatch)
        }
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(useStyles)(Step1));


//export default withStyles(useStyles)(Step1);
