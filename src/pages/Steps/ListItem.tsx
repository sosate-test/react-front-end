/** Importaciones de componentes externos */
import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';
import { ListItem, Box, Avatar } from '@material-ui/core';

import Rating from 'react-rating';
import { Star } from '@material-ui/icons';


/**Importar redux */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { accionCandidates } from '@redux/accions'




interface Props {
    classes: any,
    actions: any,
    candidates: any,
    index: any,
    item: any
}
interface State {
}


class Step1 extends Component<Props, State> {


    constructor(props: Props) {
        super(props);
        this.state = {
        }

    }



    render() {
        const { classes, candidates, actions, index, item } = this.props;

        return (
            <ListItem key={index} className={classes.containerItems} divider>

                <Box component="fieldset" mb={5} borderColor="transparent" >
                    <Avatar style={{ alignSelf: "center", }}>{item.name[0]}</Avatar>
                </Box>

                <Box component="fieldset" mb={3} borderColor="transparent">
                    <Typography component="legend">{item.name}</Typography>

                    

                    <Rating
                        emptySymbol={<Star style={{ color:'lightgray', fontSize: 50 }}/>}
                        fullSymbol={<Star style={{ color: 'rgba(252,249,30,1)',fontSize: 50 }} />}
                        initialRating={candidates.candidatesCalifated.candidates[index].rating}
                        onChange={(value)=>{
                            candidates.candidatesCalifated.candidates[index].rating = value;
                            actions.AccionCandidates.setCandidates(candidates);//modificamos redux
                        }}
                        />

                </Box>
            </ListItem>
        )

    }


}


const useStyles = () => ({
    root: {
        //width: '100%',
        justifyContent: "center",
    },
    container: {
        width: '80%',
        background: 'transparent',
        alignItems: "center"
    },
    containerItems: {
        "display": "flex",
        "flex-direction": "row",
        "justifyContent": "flex-start"
    },
    text: {
        fontSize: 14,
        paddingBottom: "3%"
    },
});



const mapStateToProps = (state: any) => {
    return {
        candidates: state.candidates
    }
}

//const mapDispatchToProps = { accionCandidates }
const mapDispatchToProps = (dispatch: any) => {
    return {
        actions: {
            AccionCandidates: bindActionCreators(accionCandidates, dispatch)
        }
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(useStyles)(Step1));


//export default withStyles(useStyles)(Step1);
