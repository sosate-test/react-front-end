/** Importaciones de componentes externos */
import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import Multiselect from 'react-widgets/lib/Multiselect'



/** Importar constantes */
import CANDIDATES from '@constants/candidates.json';


/**Importar redux */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { accionCandidates } from '@redux/accions'




interface Props {
    classes: any,
    actions: any,
    candidates: any
}
interface State {
}


class Step1 extends Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
        }
    }



    render() {
        const { classes, actions, candidates } = this.props;

        return (
            <Grid container className={classes.root}>
                <div className={classes.container}>


                    <p className={classes.text} >
                        Señor usuario por favor seleccione a continuacion los candidatos que desea calificar , al darle click en el siguiente campo se le desplegara una lista  donde podra seleccionar varios candidatos y podra filtrarlos.
                    </p>

                    <Multiselect
                        //disabled
                        data={CANDIDATES.candidates}
                        defaultValue={candidates.candidatesSeleted} //se puede dejar a si para que rentenga la informacion y no se borre
                        onChange={(value, metadata) => {

                            candidates.candidatesSeleted = value;
                            this.addCandidate(candidates, actions, metadata)
                            actions.AccionCandidates.setCandidates(candidates);

                        }}
                    />


                </div>
            </Grid>
        )
    }


    addCandidate = (candidates: any, actions: any, metadata: any) => {

        switch (metadata.action) {
            case 'remove':
                let index = metadata.lastValue.indexOf(metadata.dataItem);// obtiene el indice 
                candidates.candidatesCalifated.candidates.splice(index, 1);//eliminamos el elemento del arreglo
                actions.AccionCandidates.setCandidates(candidates);// modificamos redux
                break;
            case 'insert':
                candidates.candidatesCalifated.candidates.push({ name: metadata.dataItem, rating: null }); //agregamos un elemento nuevo
                actions.AccionCandidates.setCandidates(candidates);//modificamos redux
                break;

            default:
                break;
        }

    }



}


const useStyles = () => ({
    root: {
        //width: '100%',
        justifyContent: "center",
    },
    container: {
        width: '80%',
        background: 'transparent',
        alignItems: "center"
    },
    backButton: {
        marginRight: '5%',
    },
    text: {
        fontSize: 14,
        paddingBottom: "3%"
    },
});



const mapStateToProps = (state: any) => {
    return {
        candidates: state.candidates
    }
}

//const mapDispatchToProps = { accionCandidates }
const mapDispatchToProps = (dispatch: any) => {
    return {
        actions: {
            AccionCandidates: bindActionCreators(accionCandidates, dispatch)
        }
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(useStyles)(Step1));


//export default withStyles(useStyles)(Step1);
