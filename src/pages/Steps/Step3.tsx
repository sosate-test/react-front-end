/** Importaciones de componentes externos */
import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { toast } from 'react-toastify';


/**Importar redux */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { accionCandidates } from '@redux/accions'

import GoogleMap from './Maps/map'


interface Props {
    classes: any,
    actions: any,
    candidates: any
}
interface State {
    location: any,
    center: any,
    zoom: number
}


class Step3 extends Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            location: {},
            center: null,
            zoom: 14
        }

    }


    async componentDidMount() {
        if ("geolocation" in navigator) {
            await navigator.geolocation.getCurrentPosition((position: any) => {
                const { candidates, actions } = this.props;

                let POSITION = {
                    "lat": position.coords.latitude,
                    "lng": position.coords.longitude
                };
                this.setState({
                    center: POSITION,
                    location: POSITION
                });

                //setiamos los valores a redux
                candidates.candidatesCalifated.location = POSITION;
                candidates.candidatesCalifated.date = new Date()
                actions.AccionCandidates.setCandidates(candidates);

            },
                (err: any) => {
                    toast.error("Señor usuario, no se pudo obtener su ubicacion.");
                }
                ,
                { enableHighAccuracy: true, timeout: 5000, maximumAge: 0 }
            );
        }
    }


    render() {
        const { classes } = this.props;

        return (
            <Grid container className={classes.root}>
                <div className={classes.container}>


                    <p className={classes.text} >
                        Señor usuario a continuacion usando Google Maps , por favor ingrese un punto en el mapa.
                    </p>

                    <div style={{ height: '100%', width: '100%' }}>
                        <GoogleMap
                            googleMapURL={`https://maps.googleapis.com/maps/api/js?key=AIzaSyC3gVBF8bbxTIJwwT7c4wEid9htYf5Wreg&v=3.exp&libraries=geometry,drawing,places`}
                            loadingElement={<div style={{ height: `100%` }} />}
                            containerElement={<div style={{ height: 400}} />}
                            mapElement={<div style={{ height: `100%` }} />}

                            zoom={this.state.zoom}
                            center={this.state.center}
                            location={this.state.location}
                            onClick={this.onMapClicked}
                        />


                    </div>

                </div>
            </Grid>
        )

    }

    onMapClicked = (clickEvent: any) => {
        const { candidates, actions } = this.props;
        this.setState({ location: clickEvent.latLng });
        //setiamos los valores a redux
        candidates.candidatesCalifated.location = clickEvent.latLng;
        candidates.candidatesCalifated.date = new Date()
        actions.AccionCandidates.setCandidates(candidates);

    }

}





const useStyles = () => ({
    root: {
        //width: '100%',
        justifyContent: "center",
    },
    container: {
        width: '80%',
        background: 'transparent',
        alignItems: "center"
    },
    containerItems: {
        "display": "flex",
        "flex-direction": "row",
        "justifyContent": "flex-start"
    },
    text: {
        fontSize: 14,
        paddingBottom: "3%"
    },
});



const mapStateToProps = (state: any) => {
    return {
        candidates: state.candidates
    }
}

//const mapDispatchToProps = { accionCandidates }
const mapDispatchToProps = (dispatch: any) => {
    return {
        actions: {
            AccionCandidates: bindActionCreators(accionCandidates, dispatch)
        }
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(useStyles)(Step3));


//export default withStyles(useStyles)(Step1);
