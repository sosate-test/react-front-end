export interface Coordinate {
    lat?: number ;
    lng?: number ;
}


export function validateCoordinate(coord: Coordinate): boolean {
    return (
        coord.lat != null && coord.lng != null
    )
}
