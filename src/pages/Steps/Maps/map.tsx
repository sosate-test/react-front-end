import React, { } from 'react'
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps'


import { Coordinate } from './utils';


interface Props {
    isMarkerShown?: boolean,
    zoom?: number | 11,
    center?: Coordinate,
    location: Coordinate,
    onClick: (clickEvent: any) => any
}


const MyMapComponent = withScriptjs(withGoogleMap((props: Props) =>
    <GoogleMap
        defaultZoom={props.zoom}
        center={props.center}
        onClick={props.onClick}
    >
       
       <Marker position={props.location} />

    </GoogleMap>
))


export default MyMapComponent;