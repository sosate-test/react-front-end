/** Importaciones de componentes externos */
import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import { toast } from 'react-toastify';

/**Importar redux */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { accionCandidates } from '@redux/accions'



/** Comoponetes */
import Step1 from './Steps/Step1';
import Step2 from './Steps/Step2';
import Step3 from './Steps/Step3';




interface Props {
    classes: any,
    candidates: any,
    actions: any
}
interface State {
    activeStep: number,
    steps: Array<String>,
    skipped: any
}


class App extends Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            activeStep: 0,
            skipped: new Set(),
            steps: [
                'Seleccione los candidatos',
                'Califica los candidatos',
                'Selecciona una ubicacion'
            ]
        }
    }



    render() {
        const { classes } = this.props;
        return (
            <Grid container className={classes.root}>
                <div className={classes.container}>

                    {/* Stepeer  Header*/}
                    <Stepper activeStep={this.state.activeStep} alternativeLabel>
                        {this.state.steps.map((label, index) => (
                            <Step key={index}>
                                <StepLabel>{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>


                    <div>
                        {this.state.activeStep === this.state.steps.length ? (
                            <div>
                                <Typography className={classes.instructions}>Muchas gracias por su participacion ya hemos acabado , que tenga un buen dia.</Typography>
                                <Button onClick={this.handleReset}>Reset</Button>
                            </div>
                        ) : (
                                <div>
                                    <Typography className={classes.instructions}>{this.getStepContent(this.state.activeStep)}</Typography>
                                    <div>

                                        <Button
                                            disabled={this.state.activeStep === 0}
                                            onClick={this.handleBack}
                                            className={classes.backButton}
                                        >
                                            Back
                                        </Button>

                                        <Button variant="contained" color="primary" onClick={this.handleNext}>
                                            {this.state.activeStep === this.state.steps.length - 1 ? 'Finish' : 'Next'}
                                        </Button>
                                    </div>
                                </div>
                            )}
                    </div>
                </div>


            </Grid>
        )
    }



    handleNext = () => {

        //validamos si se puede continuar 
        let hasErro = this.validateNext();
        if (hasErro) return;

        let newSkipped = this.state.skipped;
        if (this.state.activeStep === 1) {
            newSkipped = new Set(newSkipped.values());
            newSkipped.delete(this.state.activeStep);
        }

        // validamos si esta en el ultimo paso.
        if (this.state.activeStep === this.state.steps.length - 1) {
            this.sendCandidates(newSkipped);
        } else {
            this.setState({
                activeStep: this.state.activeStep + 1,
                skipped: newSkipped
            });
        }




    };

    validateNext = () => {

        let candidates = this.props.candidates;

        switch (this.state.activeStep) {
            case 0:
                if (candidates.candidatesSeleted.length === 0) {
                    toast.info("Es necesario seleccionar primero los candidatos.");
                    return true;
                }
                break;

            case 1:
                for (let item of candidates.candidatesCalifated.candidates) {
                    if (!item.rating) {
                        toast.info(`El candidato ${item.name} se encuentra sin calificacion.`);
                        return true;
                    }
                };
                break;

            case 2:
                if (!candidates.candidatesCalifated) {
                    toast.info("Es necesario primero obtener una ubicacion en el mapa de Google primero.");
                    return true;
                }
                break;

            default:
                return false;
        }

    }

    sendCandidates = (newSkipped: any) => {
        fetch(`https://desolate-beach-06421.herokuapp.com/pusher`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                ...this.props.candidates.candidatesCalifated
            }),
        }).then((response) => response.json()).then((response) => {

            if (response.error) {
                toast.error("Lo sentimos algo salio mal, intenta de nuevo");
            } else {
                // Si esta todo bien puede seguir con su flujo
                this.setState({
                    activeStep: this.state.activeStep + 1,
                    skipped: newSkipped
                }, () => {
                    toast.success(response.response);
                });
            }

        }).catch((err) => {
            toast.error("Lo sentimos algo salio mal, intenta de nuevo");
        });
    }



    handleBack = () => {
        this.setState({ activeStep: this.state.activeStep - 1 });
    };

    handleSkip = () => {
        if (!(this.state.activeStep === 1)) {
            // You probably want to guard against something like this,
            // it should never occur unless someone's actively trying to break something.
            throw new Error("You can't skip a step that isn't optional.");
        }

        this.setState({ activeStep: this.state.activeStep + 1 });
        const newSkipped = new Set(this.state.skipped.values());
        newSkipped.add(this.state.activeStep);

        this.setState({
            skipped: newSkipped
        });

    };

    handleReset = () => {
        const { candidates, actions } = this.props;
        actions.AccionCandidates.resetAll(candidates);
        this.setState({ activeStep: 0 });
    };


    getStepContent(stepIndex: number) {
        switch (stepIndex) {
            case 0:
                return <Step1 />;
            case 1:
                return <Step2 />;
            case 2:
                return <Step3 />;
            default:
                return 'Unknown stepIndex';
        }
    }


}


const useStyles = () => ({
    root: {
        //width: '100%',
        justifyContent: "center",
    },
    container: {
        width: '80%',
        background: 'transparent',
        alignItems: "center"
    },
    backButton: {
        marginRight: '5%',
    },
    instructions: {
        marginTop: '5%',
        marginBottom: '5%',
    },
});



const mapStateToProps = (state: any) => {
    return {
        candidates: state.candidates
    }
}

//const mapDispatchToProps = { accionCandidates }
const mapDispatchToProps = (dispatch: any) => {
    return {
        actions: {
            AccionCandidates: bindActionCreators(accionCandidates, dispatch)
        }
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(useStyles)(App));

//export default withStyles(useStyles)(App);
