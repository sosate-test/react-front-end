import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from '@pages/App';
import * as serviceWorker from './serviceWorker';

import 'react-widgets/dist/css/react-widgets.css';
import 'react-toastify/dist/ReactToastify.css';

import { Provider } from 'react-redux';
import store from '@redux/store';

import { ToastContainer } from 'react-toastify';



const rootElement = document.getElementById('root')
ReactDOM.render(
  <React.StrictMode>
    
    <Provider store={store}>
      <App />
    </Provider>

    <ToastContainer
      position="top-right"
      autoClose={5000}
      hideProgressBar={false}
      newestOnTop={false}
      closeOnClick
      rtl={false}
      pauseOnFocusLoss
      draggable
      pauseOnHover
    />

  </React.StrictMode>,
  rootElement
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
