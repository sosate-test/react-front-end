//import InitialState from '../store/initialState';
//estado inicial del reduer Control de Inicio
var InitialState = {
    candidatesSeleted:[],
    candidatesCalifated: { candidates: [], location: null, date: null},
    candidatesLocated:{}
}


const reducer = (state = InitialState, action : any) => {
    switch (action.type) {
        case 'setCandidates':

            // console.log(action);
            // modificamos lo que necesitamos
            state = action.payload.data;
            // console.log(state)

            // retornamos el nuevo state
            return state;
        case 'resetAll':

            // console.log(action);
            // modificamos lo que necesitamos
            state = {
                candidatesSeleted: [],
                candidatesCalifated: { candidates: [], location: null, date: null},
                candidatesLocated: {}
            };
            // console.log(state)

            // retornamos el nuevo state
            return state;
        default:
            // si el action.type no existe o no concuerda
            // con ningunos de los casos definidos
            // devolvemos el estado sin modificar
            return state;
    }

};


export default reducer;
