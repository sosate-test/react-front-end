
/**
 * Devuelve una acción de tipo ADD_TODO
 * @param  {String} text Texto del TODO
 * @return {Object}      Objecto de acción
 */


export function setCandidates(data : any) {
    return {
        type: 'setCandidates',
        payload: {
            data,
        },
    };
}


export function resetAll(data: any) {
    return {
        type: 'resetAll',
        payload: {
            data,
        },
    };
}
